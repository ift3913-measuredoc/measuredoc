package Analyser;

import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.stmt.IfStmt;
import com.github.javaparser.ast.stmt.Statement;
import com.github.javaparser.ast.stmt.SwitchEntry;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.util.List;

/**
 * VisitorCollector contient des methodes calculant des métriques qui sont utiles
 * autant pour calculer des métriques concernant les classes (dans ClassCollector)
 * que pour calculer des métriques concernant les méthodes (dans MethodCollector).
 * */
public abstract class VisitorCollector extends VoidVisitorAdapter<List<String[]>> {

    /**
     * Calcul le nombre de noeuds predicats, soit les noeuds
     * ayant un degre de sortie superieur a 1.
     *
     * @param statements tout statement se trouvant dans une méthode.
     * @return nombre de noeuds predicats se trouvant dans la liste de statements.
     * */
    public float numOfPredicatNodes(NodeList<Statement> statements){
        float result =  0.0f;

        for(Statement methodStatement : statements){
            if(methodStatement.isIfStmt()){
                result++;

                IfStmt methodIfStatement = methodStatement.asIfStmt();
                while(methodIfStatement.hasCascadingIfStmt()){
                    result++;

                    methodIfStatement = methodIfStatement.getElseStmt().get().asIfStmt();

                    if(methodIfStatement.hasThenBlock()){
                        result += numOfPredicatNodes(methodIfStatement.getThenStmt().asBlockStmt().getStatements());
                    }

                    if(methodIfStatement.hasElseBlock()){
                        result += numOfPredicatNodes(
                                methodIfStatement.getElseStmt().get().asBlockStmt().getStatements());
                    }
                }

                if(methodStatement.asIfStmt().hasThenBlock()){
                    result += numOfPredicatNodes(
                            methodStatement.asIfStmt().getThenStmt().asBlockStmt().getStatements());
                }

                if(methodStatement.asIfStmt().hasElseBlock()){
                    result += numOfPredicatNodes(
                            methodStatement.asIfStmt().getElseStmt().get().asBlockStmt().getStatements());
                }
            }
            else if(methodStatement.isWhileStmt()){
                result++;
                if(!methodStatement.asWhileStmt().getBody().isEmptyStmt()){
                    result += numOfPredicatNodes(methodStatement.asWhileStmt().getBody().asBlockStmt().getStatements());
                }

            }
            else if(methodStatement.isSwitchStmt()){
                // TODO: calculate default and block statements in switch case
                for(SwitchEntry entry : methodStatement.asSwitchStmt().getEntries()){
                    result++;
                }
            }
        }
        return result;
    }

    /**
     * Calcul la somme pondérée des complexités des méthodes d'une classe.
     *
     * Si toutes les méthodes d'une classe ont un complexité de 1, elle est
     * égale au nombre de méthodes
     *
     * @param classOrInterface un èlèment classe ou interface d'un fichier java parser.
     * @return weighted methods per class metrics
     * */
    public float weightedMethodsPerClass(ClassOrInterfaceDeclaration classOrInterface){
        List<MethodDeclaration> methods = classOrInterface.getMethods();
        float result = 0.0f;
        for(MethodDeclaration method : methods){
            if(!method.getBody().isEmpty()){
                result += numOfPredicatNodes(method.getBody().get().getStatements());
            }
            result += 1.0f;
        }
        return result;
    }

    /**
     * Calcul le nombre de lignes de code, excluant les commentaires, d'une méthode.
     *
     * @param method un élément méthode d'un fichier java parser.
     * @return nombre de lignes de code
     * */
    public float methodLOC(MethodDeclaration method){
        float linesCount = 1.0f; // Declaration
        float methodRange = 0.0f;
        if(!method.getBody().isEmpty()){
            List<Statement> statements = method.getBody().get().getStatements();
            for(Statement statement : statements){
                statement.removeComment();
                float lineCount = (float) statement.getRange().get().getLineCount();
                linesCount += lineCount;
            }
            methodRange = (float) method.getRange().get().getLineCount();
            methodRange = (methodRange > 1) ? 1.0f : 0.0f; // Assume that method declaration is well formatted
        }
        return (linesCount + methodRange);
    }

    /**
     * Calcul le nombre de lignes de code, excluant les commentaires, d'un constructeur.
     *
     * @param constructor un élément constructeur d'un fichier java parser.
     * @return nombre de lignes de code
     * */
    public float constructorLOC(ConstructorDeclaration constructor){
        List<Statement> statements = constructor.getBody().getStatements();
        float linesCount = 1.0f; // Declaration
        for(Statement statement : statements){
            statement.removeComment();
            float lineCount = (float) statement.getRange().get().getLineCount();
            linesCount += lineCount;
        }
        float methodRange = (float) constructor.getRange().get().getLineCount();
        methodRange = (methodRange > 1) ? 1.0f : 0.0f; // Assume that method declaration is well formatted

        return (linesCount + methodRange);
    }
}
