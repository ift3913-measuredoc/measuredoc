package Analyser;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.*;

import java.util.List;

/**
 * ClassCollector visit toutes les classes et interfaces d'un fichier donné et
 * y calcul les métriques spécifiées (LOC, CLOC, BC, WCM et DC).
 * */
public class ClassCollector extends VisitorCollector {

    /**
     * Visite tous les enfants du Compilation Unit qui sont des ClassOrInterfaceDeclaration
     * et calcul les métriques spécifiées et les inscrit dans une List de String[].
     *
     * @param classOrInterface un èlèment classe ou interface d'un fichier java parser.
     * @param collector une liste de tableau de String servant à la création du fichier CSV
     * */
    @Override
    public void visit(ClassOrInterfaceDeclaration classOrInterface, List<String[]> collector){
        super.visit(classOrInterface, collector);

        String filePath = collector.get(1)[0];

        // Return number of lines of code and comments of a class
        float nodeComment = 0.0f;
        if(!classOrInterface.getComment().isEmpty()){ // if a comment is at the beginning of a class
            nodeComment = classOrInterface.getComment().get().getRange().get().getLineCount();
        }
        float classeCLOC = (float) classOrInterface.getRange().get().getLineCount() + nodeComment;

        // Return lines of code of a class
        float classeLOC = classLOC(classOrInterface);

        // Weighted Methods Per Class metric
        float wmc = weightedMethodsPerClass(classOrInterface);

        collector.add(new String[]{
                filePath,
                classOrInterface.getName().asString(),
                String.valueOf(Math.ceil(classeLOC)),
                String.valueOf(Math.ceil(classeCLOC)),
                String.valueOf(classeCLOC/classeLOC), // comment density for a class
                String.valueOf(Math.ceil(wmc)),
                String.valueOf(classeCLOC/(classeLOC*wmc)) // degree to which a class is well commented
        });
    }

    /**
     * Calcul le nombre de lignes de code, excluant les commentaires, d'une classe.
     *
     * @param classe un élément classe d'un fichier java parser
     * @return nombre de lignes de code
     * */
    public float classLOC(ClassOrInterfaceDeclaration classe){
        // Get num of lines of code of all methods
        List<MethodDeclaration> methods = classe.getMethods();
        float sumOfMethodsLOC = 0.0f;
        for(MethodDeclaration method : methods){
            sumOfMethodsLOC += methodLOC(method);
        }

        // Get num of lines of code of all constructors
        List<ConstructorDeclaration> constructors = classe.getConstructors();
        float sumOfConstructorLOC = 0.0f;
        for(ConstructorDeclaration constructor : constructors){
            sumOfConstructorLOC += constructorLOC(constructor);
        }

        NodeList<BodyDeclaration<?>> bodyDeclarations = classe.getMembers();
        float linesCount = 1.0f; // Count declaration of the class
        for(BodyDeclaration<?> bodyDeclaration : bodyDeclarations){
            if(!(bodyDeclaration.isMethodDeclaration() || bodyDeclaration.isConstructorDeclaration())){
                bodyDeclaration.removeComment();
                bodyDeclaration.getAllContainedComments().forEach(Node::removeForced);
                float lineCount = (float) bodyDeclaration.getRange().get().getLineCount(); // TODO: review this
                linesCount += lineCount;
            }
        }
        float classRange = (float) classe.getRange().get().getLineCount();
        classRange = (classRange > 0) ? 1.0f : 0.0f; // Assume that method declaration is well formatted

        return sumOfMethodsLOC + sumOfConstructorLOC + linesCount + classRange;
    }
}