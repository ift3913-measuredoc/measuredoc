package Analyser;

import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;

import java.util.List;

/**
 * MethodCollector visit toutes les méthodes d'un fichier donné et
 * y calcul les métriques spécifiées (LOC, CLOC, BC, WCM et DC).
 * */
public class MethodCollector extends VisitorCollector {

    /**
     * Visite tous les enfants du Compilation Unit qui sont des ClassOrInterfaceDeclaration
     * et, pour chacune des classes, visite toutes les méthodes et y calcul les métriques
     * spécifiées et les inscrit dans une List de String[].
     *
     * @param classOrInterface un èlèment classe ou interface d'un fichier java parser.
     * @param collector une liste de tableau de String servant à la création du fichier CSV
     * */
    @Override
    public void visit(ClassOrInterfaceDeclaration classOrInterface, List<String[]> collector) {
        super.visit(classOrInterface, collector);

        String filePath = collector.get(1)[0];

        List<MethodDeclaration> methods = classOrInterface.getMethods();
        for(MethodDeclaration method : methods) {

            // Return number of lines of code and comments of a method
            float nodeComment = 0.0f;
            if(!method.getComment().isEmpty()){ // if a comment is at the beginning of a method
                nodeComment = method.getComment().get().getRange().get().getLineCount();
            }
            float methode_CLOC = (float) method.getRange().get().getLineCount() + nodeComment;

            // Return lines of code of a method
            float methode_LOC = methodLOC(method);

            // Complexity Cyclomatic metric
            float cc = 1.0f;
            if(!method.getBody().isEmpty()){
                cc += numOfPredicatNodes(method.getBody().get().getStatements());
            }

            collector.add(new String[] {
                    filePath,
                    classOrInterface.getName().asString(),
                    prettySignature(method.getSignature().toString()),
                    String.valueOf(Math.ceil(methode_LOC)),
                    String.valueOf(Math.ceil(methode_CLOC)),
                    String.valueOf(methode_CLOC/methode_LOC), // comment density for a method
                    String.valueOf(Math.ceil(cc)),
                    String.valueOf(methode_CLOC/(methode_LOC*cc)) // degree to which a method is well commented
            });
        }
    }

    /**
     * Reformat la signature de la methode dans le format
     * "nomDeLaMethode_typeArg1_typeArg2...".
     *
     * @param methodSignature signature d'une méthode.
     * @return signature de la methode
     * */
    public String prettySignature(String methodSignature){
        String signature = methodSignature.substring(0, methodSignature.indexOf("("));

        String args = methodSignature.substring(methodSignature.indexOf("(") + 1, methodSignature.indexOf(")"));
        for(String arg : args.split(",")){
            if(arg.length() == 0) break;
            signature = signature.concat("_" + arg.strip());
        }

        return signature;
    }
}
