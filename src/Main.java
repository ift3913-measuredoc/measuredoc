import Analyser.ClassCollector;
import Analyser.MethodCollector;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.visitor.VoidVisitor;
import com.opencsv.CSVWriter;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;
import java.util.regex.*;

public class Main {
    private static final String REGEX = "[\\w]*(\\.java)";  // Pattern to find .java files

    private static final String TEMP = "temp.java";
    private static final String CLASSES = "classes.csv";
    private static final String METHODES = "methodes.csv";

    /**
     * À l'exécution, il traverse en entier le projet donnée en argument
     * et y calcul les métriques. Il retourne deux fichiers CSV contenant
     * les métriques concernant le projet.
     *
     * @param args : chemin d'accès absolu d'un projet en Java.
     * @throws IOException
     * */
    public static void main(String[] args) throws IOException {
        Pattern pattern = Pattern.compile(REGEX);

        List<String[]> classesCSV = new ArrayList<>();
        List<String[]> methodesCSV = new ArrayList<>();

        if (args.length != 1) {
            System.err.println("Bad argument; Need directory path.");
        }

        if(Files.exists(Paths.get(CLASSES))){
            System.err.println(CLASSES + " already exits.");
            throw new FileAlreadyExistsException(CLASSES);
        }

        if(Files.exists(Paths.get(METHODES))){
            System.err.println(METHODES + " already exits.");
            throw new FileAlreadyExistsException(METHODES);
        }

        String path = args[0];

        // Headers of CLASSES and METHODES files
        classesCSV.add(0, new String[] {"chemin", "class", "classe_LOC",
                "classe_CLOC", "classe_DC", "WMC", "classe_BC"});
        methodesCSV.add(0, new String[] {"chemin", "class", "methode", "methode_LOC",
                "methode_CLOC", "methode_DC", "CC", "methode_BC"});

        try {
            Stream<Path> paths = Files.walk(Paths.get(path));
            paths.map(Path::toString)
                    .forEach(toString -> {
                        Matcher matcher = pattern.matcher(toString);

                        // Find absolute path of .java files
                        if(matcher.find()){
                            String filePath = toString;

                            classesCSV.add(1, new String[] {filePath});
                            methodesCSV.add(1, new String[] {filePath});

                            try {
                                // Remove all empty line from temp.java file
                                File temp = new File(TEMP);
                                Scanner scanner = new Scanner(new File(filePath));
                                BufferedWriter bw = new BufferedWriter(new FileWriter(temp));

                                while(scanner.hasNextLine()){
                                    String line = scanner.nextLine();
                                    if(StringUtils.isNotBlank(line)){
                                        bw.write(line);
                                        bw.write(System.getProperty("line.separator"));
                                    }
                                }
                                scanner.close();
                                bw.flush();
                                bw.close();

                                // Parse file
                                CompilationUnit cu = StaticJavaParser.parse(temp);

                                // Visit all classes of a specific file
                                VoidVisitor<List<String[]>> classVisitor = new ClassCollector();
                                classVisitor.visit(cu, classesCSV);

                                // Write metrics of classes in CLASSES file
                                Writer writerClass = Files.newBufferedWriter(Paths.get(CLASSES));
                                CSVWriter csvWriterClass = new CSVWriter(writerClass);
                                classesCSV.remove(1);
                                csvWriterClass.writeAll(classesCSV);

                                csvWriterClass.flush();
                                csvWriterClass.close();

                                // Visit all methods of a specific file
                                VoidVisitor<List<String[]>> methodVisitor = new MethodCollector();
                                methodVisitor.visit(cu, methodesCSV);

                                // Write metrics of methods in METHODES file
                                Writer writerMethod = Files.newBufferedWriter(Paths.get(METHODES));
                                CSVWriter csvWriterMethod = new CSVWriter(writerMethod);
                                methodesCSV.remove(1);
                                csvWriterMethod.writeAll(methodesCSV);

                                csvWriterMethod.flush();
                                csvWriterMethod.close();

                                temp.delete();

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });

        } catch (NoSuchFileException e) {
            System.err.println("Bad argument; Need directory path.");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}