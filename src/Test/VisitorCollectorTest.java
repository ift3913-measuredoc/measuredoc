package Test;

import Analyser.VisitorCollector;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VisitorCollectorTest extends VisitorCollector {

    @Test
    void numOfPredicatNodesTest() {
        // Testing If-ElseIf-Else
        CompilationUnit cu = StaticJavaParser.parse(
                "public class Test {\n" +
                        "        public void testing(){\n" +
                        "            if (a < b){\n" +
                        "                while(a < b){\n" +
                        "                    a++;\n" +
                        "                }\n" +
                        "            }\n" +
                        "            else if (b == a){\n" +
                        "                while(b != a){\n" +
                        "                    a = b;\n" +
                        "                }\n" +
                        "\n" +
                        "                if(b < a){\n" +
                        "                    a = b;\n" +
                        "                }\n" +
                        "                else{\n" +
                        "                    b = a;\n" +
                        "                }\n" +
                        "\n" +
                        "            }\n" +
                        "            else{\n" +
                        "                while(a < b){\n" +
                        "                    b++;\n" +
                        "                }\n" +
                        "                String hello = \"blah\";\n" +
                        "            }\n" +
                        "        }\n" +
                        "    }"
        );

        MethodDeclaration method = cu.getClassByName("Test").get().getMethods().get(0);
        assertEquals(6, numOfPredicatNodes(method.getBody().get().getStatements()));

        // Testing nested predicat
        cu = StaticJavaParser.parse(
                "public class Test {\n" +
                        "        public void testing(){\n" +
                        "            while (month < 10){\n" +
                        "                if (testing == \"March\"){\n" +
                        "                    break;\n" +
                        "                }\n" +
                        "\n" +
                        "                while(testing != \"March\"){\n" +
                        "                    month++;\n" +
                        "                }\n" +
                        "                month++;\n" +
                        "            }\n" +
                        "        }\n" +
                        "    }"
        );

        method = cu.getClassByName("Test").get().getMethods().get(0);
        assertEquals(3, numOfPredicatNodes(method.getBody().get().getStatements()));

        // Testing Switch-Case
        cu = StaticJavaParser.parse(
                "public class Test {\n" +
                        "        public void testing(){\n" +
                        "            switch (month) {\n" +
                        "                case 1:  monthString = \"January\";\n" +
                        "                    break;\n" +
                        "                case 2:  monthString = \"February\";\n" +
                        "                    break;\n" +
                        "                case 3:  monthString = \"March\";\n" +
                        "                    break;\n" +
                        "                case 4:  monthString = \"April\";\n" +
                        "                    break;\n" +
                        "                case 5:  monthString = \"May\";\n" +
                        "                    break;\n" +
                        "                case 6:  monthString = \"June\";\n" +
                        "                    break;\n" +
                        "                case 7:  monthString = \"July\";\n" +
                        "                    break;\n" +
                        "                case 8:  monthString = \"August\";\n" +
                        "                    break;\n" +
                        "                case 9:  monthString = \"September\";\n" +
                        "                    break;\n" +
                        "                case 10: monthString = \"October\";\n" +
                        "                    break;\n" +
                        "                case 11: monthString = \"November\";\n" +
                        "                    break;\n" +
                        "                case 12: monthString = \"December\";\n" +
                        "                    break;\n" +
                        "                default: monthString = \"Invalid month\";\n" +
                        "                    break;\n" +
                        "            }\n" +
                        "        }\n" +
                        "    }"
        );

        // This test fails ; our code doesn't treat 'default' as an 'else' but rather as a 'if'.
        method = cu.getClassByName("Test").get().getMethods().get(0);
        assertEquals(12, numOfPredicatNodes(method.getBody().get().getStatements()));
    }

    @Test
    void weightedMethodsPerClassTest() {
        CompilationUnit cu = StaticJavaParser.parse(
                "public class Test {\n" +
                        "        int value;\n" +
                        "        String month;\n" +
                        "        \n" +
                        "        public void testing(){\n" +
                        "            while (value < 10){\n" +
                        "                if (month == \"March\"){\n" +
                        "                    break;\n" +
                        "                }\n" +
                        "\n" +
                        "                while(month != \"March\"){\n" +
                        "                    value++;\n" +
                        "                }\n" +
                        "                value++;\n" +
                        "            }\n" +
                        "        }\n" +
                        "\n" +
                        "        public int getValue() {\n" +
                        "            return value;\n" +
                        "        }\n" +
                        "        \n" +
                        "        public void setValue(){}\n" +
                        "    }"
        );

        ClassOrInterfaceDeclaration classDeclaration = cu.getClassByName("Test").get();
        assertEquals(6, weightedMethodsPerClass(classDeclaration));
    }

    @Test
    void methodLOCTest() {
        CompilationUnit cu = StaticJavaParser.parse(
                        "    public class Test {\n" +
                        "        public void setValue(){\n" +
                        "            /*\n" +
                        "             * This is a bloc of comment\n" +
                        "             * */\n" +
                        "        }\n" +
                        "    }"
        );

        MethodDeclaration method = cu.getClassByName("Test").get().getMethods().get(0);
        assertEquals(2, methodLOC(method));

        cu = StaticJavaParser.parse(
                "public class Test {\n" +
                        "        /*lots\n" +
                        "        and lots and lots of code\n" +
                        "        * */\n" +
                        "        int value;\n" +
                        "        String month;\n" +
                        "\n" +
                        "        /**\n" +
                        "         * This function set an int value to num.\n" +
                        "         *\n" +
                        "         * @return void\n" +
                        "         */\n" +
                        "        public void testing() {\n" +
                        "            while (value < 10) { // aaah! a comment in a line of code\n" +
                        "                if (month == \"March\") {\n" +
                        "                    break;\n" +
                        "                }\n" +
                        "\n" +
                        "                // This is a comment\n" +
                        "\n" +
                        "                while (month != \"March\") {\n" +
                        "                    value++;\n" +
                        "                }\n" +
                        "                value++;\n" +
                        "            }\n" +
                        "        }\n" +
                        "    }"
        );

        // This test fails
        method = cu.getClassByName("Test").get().getMethods().get(0);
        assertEquals(11, methodLOC(method));
    }

    @Test
    void constructorLOCTest() {
        CompilationUnit cu = StaticJavaParser.parse(
                "public class Test {\n" +
                        "        int num;\n" +
                        "\n" +
                        "        // This is a comment\n" +
                        "\n" +
                        "        public ClassTest() {\n" +
                        "            /*\n" +
                        "             * This is a bloc of comment\n" +
                        "             * */\n" +
                        "            num = 30;\n" +
                        "        }\n" +
                        "    }"
        );

        ConstructorDeclaration constructor = cu.getClassByName("Test").get().getConstructors().get(0);
        assertEquals(3, constructorLOC(constructor));

        cu = StaticJavaParser.parse(
                "public class Test {\n" +
                        "        public ClassTest() {}\n" +
                        "}"
        );

        constructor = cu.getClassByName("Test").get().getConstructors().get(0);
        assertEquals(1, constructorLOC(constructor));
    }
}