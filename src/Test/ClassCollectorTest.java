package Test;

import Analyser.ClassCollector;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ClassCollectorTest extends ClassCollector {

    @Test
    void classLOCTest() {
        CompilationUnit cu = StaticJavaParser.parse(
                "/**\n" +
                        "     * This is a Javadoc comment for the class Test\n" +
                        "     **/\n" +
                        "    public class Test {\n" +
                        "        /*lots\n" +
                        "        and lots and lots of code\n" +
                        "        * */\n" +
                        "        int value;\n" +
                        "        String month;\n" +
                        "\n" +
                        "        /**\n" +
                        "         * This function set an int value to num.\n" +
                        "         *\n" +
                        "         * @return void\n" +
                        "         */\n" +
                        "        public void testing(){\n" +
                        "            while (value < 10){ // aaah! a comment in a line of code\n" +
                        "                if (month == \"March\"){\n" +
                        "                    break;\n" +
                        "                }\n" +
                        "\n" +
                        "                // This is a comment\n" +
                        "\n" +
                        "                while(month != \"March\"){\n" +
                        "                    value++;\n" +
                        "                }\n" +
                        "                value++;\n" +
                        "            }\n" +
                        "        }\n" +
                        "\n" +
                        "        public int getValue() {\n" +
                        "            return value;\n" +
                        "        }\n" +
                        "\n" +
                        "        public void setValue(){\n" +
                        "            /*\n" +
                        "             * This is a bloc of comment\n" +
                        "             * */\n" +
                        "        }\n" +
                        "    }"
        );

        // Testing fail, due to `methodLOC`.
        ClassOrInterfaceDeclaration classDeclaration = cu.getClassByName("Test").get();
        assertEquals(20, classLOC(classDeclaration));
    }
}