package Test;

import Analyser.MethodCollector;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.MethodDeclaration;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MethodCollectorTest extends MethodCollector {

    @Test
    void prettySignatureTest() {
        CompilationUnit cu = StaticJavaParser.parse(
                "public class Test {\n" +
                        "        public void setNum(int value, int value1, int value2){\n" +
                        "        /*lots\n" +
                        "        and lots and lots of code\n" +
                        "        * */\n" +
                        "            int j = 0;\n" +
                        "            while(j < 2){\n" +
                        "                int i = 0;\n" +
                        "                if (i == j){\n" +
                        "                    this.num = value;\n" +
                        "                }\n" +
                        "\n" +
                        "\n" +
                        "                j++;\n" +
                        "            }\n" +
                        "            this.num = value;\n" +
                        "        }\n" +
                        "    }"
        );

        MethodDeclaration method = cu.getClassByName("Test").get().getMethods().get(0);
        assertEquals("setNum_int_int_int", prettySignature(method.getSignature().toString()));

        cu = StaticJavaParser.parse(
                "public class Test {\n" +
                        "        public void hello(){}" +
                        "}"
        );

        method = cu.getClassByName("Test").get().getMethods().get(0);
        assertEquals("hello", prettySignature(method.getSignature().toString()));

        cu = StaticJavaParser.parse(
                "public class Test {\n" +
                        "        public static void main (String[] args){}" +
                        "}"
        );

        method = cu.getClassByName("Test").get().getMethods().get(0);
        assertEquals("main_String[]", prettySignature(method.getSignature().toString()));

        cu = StaticJavaParser.parse(
                "public class Test {\n" +
                        "        public static void main (String[] args, String name, int value){}" +
                        "}"
        );

        method = cu.getClassByName("Test").get().getMethods().get(0);
        assertEquals("main_String[]_String_int", prettySignature(method.getSignature().toString()));

    }
}