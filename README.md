# MeasureDoc

MeasureDoc est un programme permettant de mesurer la sous-documentation d'un projet en Java et
ainsi permettre de trouver les cas où les développeurs ont créé du code compliqué sans un
niveau de documentation adéquat.  

## Exigences
MeasureDoc requiert une version de JDK 1.11.0 ou supérieure.

## Usage

MeasureDoc prend en paramètre le chemin d'accès absolu du projet et retourne deux fichiers CSV, 
`classes.csv` et  `methodes.csv`, contenant des informations concernant les métriques des méthodes
et des classes.

Pour exécuter le programme, ouvrez un terminal où se trouve le projet et rouler la commande :

```bash
java -jar measuredoc.jar <chemin d'accès absolu du projet>
```

---
#### classes.csv
```
chemin, class, classe_LOC, classe_CLOC, classe_DC, WMC, classe_BC
```
* chemin : chemin d'accès absolu du fichier contenant la classe.
* class : signature de la classe.
* classe_LOC : nombre de lignes de code d'une classe excluant les commentaires.
* classe_CLOC : nombre de lignes de code d'un classe incluant les commentaires.
* classe_DC : densite de commentaires d'une classe.
* WMC : somme pondérée des complexités des methodes d'une classe.
* classe_BC : degré selon lequel la classe est bien commentée.

---
#### methodes.csv
```
chemin, class, methode,  methode_LOC, methode_CLOC, methode_DC, CC, methode_BC
```
* chemin : chemin d'accès absolu du fichier contenant la méthode.
* class : signature de la classe auquel la méthode appartient.
* methode : signature de la méthode.
* methode_LOC : nombre de lignes de code d'une méthode excluant les commentaire.
* methode_CLOC : nombre de lignes de code d'une méthode incluant les commentaire.
* methode_DC : densité de commentaires d'une méthode.
* CC : complexité cyclomatique de McCabe de la méthode.
* methode_BC : degré selon lequel la méthode est bien commentée.

---

## Précision

### Mesure
Le programme ne prend pas en compte tous ce qui se trouve au dessus des déclarations de classes, à l'exception des
commentaires, (ex. `import`, `package`) dans la mesure des métriques de classe;  on considère qu'elles font partie de la taille physique
du code et non des classes.

Dans le calcul des lignes de code sans commentaire, on assume, autant pour les classes que pour les méthodes, qu'elles
sont bien formattées, c'est-à-dire, soit qu'elles sont de la forme :

```
public void methodExample(int arg){
    return;
}
```

ou, si elle n'ont pas de corps, de la forme suivante :

```
public void methodExample(int arg);
```